<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;

class UpdateUserController extends Controller
{
    private $updateUserController;

    public function __construct(\Src\BoundedContext\User\Infrastructure\UpdateUserController $updateUserController)
    {
        $this->updateUserController = $updateUserController;
    }

    public function __invoke(Request $request)
    {
    $updateUser = new UserResource($this->updateUserController->__invoke($request));
    return response($updateUser,200);
    }
}
