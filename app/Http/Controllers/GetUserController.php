<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response ;

class GetUserController extends Controller
{
    private $getUserController;

    public function __construct(\Src\BoundedContext\User\Infrastructure\GetUserController $getUserController)
    {
        $this->getUserController = $getUserController;
    }

    public function __invoke(Request $request)
    {
        $user = new UserResource($this->getUserController->__invoke($request));
        return response($user, Response::HTTP_OK);
    }
}
