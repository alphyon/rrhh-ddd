<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_new_user_created(): void
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password
        ];
        $this->postJson('api/user', $data)
            ->assertStatus(201);
        $this->assertCount(1, User::all());
        $user = User::first();

        $this->json('GET', 'api/user/' . $user->id)
            ->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'name' => $data['name'],
                        'email' => $data['email']
                    ]
                ]
            );
    }

}
