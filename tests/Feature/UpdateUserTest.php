<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function test_a_user_is_update()
    {
        $user = User::factory()->create(['email_verified_at' => null]);
        $update = [
            'name' => $this->faker->name,
            'email' => $this->faker->email
        ];
        $a =$this->putJson('api/user/' . $user->id, [
            'data' => [
                'name' => $update['name'],
                'email'=> $update['email']
            ]
        ]);
        $a->assertStatus(200);

        $b =$this->json('GET', 'api/user/' . $user->id);
            $b->assertStatus(200)
            ->assertJson([
                'data' => [
                    'name' => $user->name,
                    'email' => $user->email,
                    'emailVerifiedDate' => $user->email_verified_at
                ]
            ]);

    }
}
