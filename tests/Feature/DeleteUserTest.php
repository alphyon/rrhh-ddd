<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
 use RefreshDatabase;
 public function test_a_user_is_delete()
 {
     $user = User::factory()->create();
     $this->assertCount(1,User::all());

     $this->json('DELETE','api/user/'.$user->id)
         ->assertStatus(204);

     $this->assertCount(0,User::all());
 }
}
