<?php

namespace Src\BoundedContext\User\Domain\Contracts;

use Src\BoundedContext\User\Domain\User;
use Src\BoundedContext\User\Domain\ValueObject\UserEmail;
use Src\BoundedContext\User\Domain\ValueObject\UserId;
use Src\BoundedContext\User\Domain\ValueObject\UserName;

/**
 *
 */
interface UserRepositoryContract
{
    /**
     * @param UserId $id
     * @return User|null
     */
    public function find(UserId $id): ?User;

    /**
     * @param UserName $userName
     * @param UserEmail $userEmail
     * @return User|null
     */
    public function findByCriteria(UserName $userName, UserEmail $userEmail): ?User;

    /**
     * @param User $user
     * @return void
     */
    public function save(User $user): void;

    /**
     * @param UserId $userId
     * @param User $user
     * @return void
     */
    public function update(UserId $userId, User $user): void;

    /**
     * @param UserId $id
     * @return void
     */
    public function delete(UserId $id): void;
}
