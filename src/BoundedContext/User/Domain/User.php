<?php

namespace Src\BoundedContext\User\Domain;

use Src\BoundedContext\User\Domain\ValueObject\UserEmail;
use Src\BoundedContext\User\Domain\ValueObject\UserEmailVerifiedDate;
use Src\BoundedContext\User\Domain\ValueObject\UserName;
use Src\BoundedContext\User\Domain\ValueObject\UserPassword;
use Src\BoundedContext\User\Domain\ValueObject\UserRememberToken;

/**
 *
 */
final class User
{
    /**
     * @var UserName
     */
    private $name;
    /**
     * @var UserEmail
     */
    private $email;
    /**
     * @var UserEmailVerifiedDate
     */
    private $emailVerifiedDate;
    /**
     * @var UserPassword
     */
    private $password;
    /**
     * @var UserRememberToken
     */
    private $rememberToken;


    /**
     * @param UserName $name
     * @param UserEmail $email
     * @param UserEmailVerifiedDate $emailVerifiedDate
     * @param UserPassword $password
     * @param UserRememberToken $rememberToken
     */
    public function __construct(
        UserName              $name,
        UserEmail             $email,
        UserEmailVerifiedDate $emailVerifiedDate,
        UserPassword          $password,
        UserRememberToken     $rememberToken)
    {
        $this->name = $name;
        $this->email = $email;
        $this->emailVerifiedDate = $emailVerifiedDate;
        $this->password = $password;
        $this->rememberToken = $rememberToken;
    }

    /**
     * @return UserName
     */
    public function name(): UserName
    {
        return $this->name;
    }

    /**
     * @return UserEmail
     */
    public function email(): UserEmail
    {
        return $this->email;
    }

    /**
     * @return UserEmailVerifiedDate
     */
    public function emailVerifiedDate(): UserEmailVerifiedDate
    {
        return $this->emailVerifiedDate;
    }

    /**
     * @return UserPassword
     */
    public function password(): UserPassword
    {
        return $this->password;
    }

    /**
     * @return UserRememberToken
     */
    public function rememberToken(): UserRememberToken
    {
        return $this->rememberToken;
    }

    /**
     * @param UserName $name
     * @param UserEmail $email
     * @param UserEmailVerifiedDate $emailVerifiedDate
     * @param UserPassword $password
     * @param UserRememberToken $rememberToken
     * @return User
     */
    public static function create(
        UserName              $name,
        UserEmail             $email,
        UserEmailVerifiedDate $emailVerifiedDate,
        UserPassword          $password,
        UserRememberToken     $rememberToken
    ): User
    {
        $user = new self(
            $name,
            $email,
            $emailVerifiedDate,
            $password,
            $rememberToken
        );

        return $user;
    }


}
