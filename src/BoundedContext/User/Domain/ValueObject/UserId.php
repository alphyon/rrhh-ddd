<?php

namespace Src\BoundedContext\User\Domain\ValueObject;

use http\Exception\InvalidArgumentException;

class UserId
{
    private $value;
public function __construct(int $id)
{
    $this->validate($id);
    $this->value = $id;
}

    public function value():int
    {
        return $this->value;
    }

    private function validate(int $id):void
    {
        $options = [
            'options'=>[
                'min_range'=>1
            ]
        ];

        if(!filter_var($id,FILTER_VALIDATE_INT,$options)){
            throw new InvalidArgumentException(
                sprintf('<%s> does not allow the value <%s>',static::class,$id)
            );
        }
    }
}
