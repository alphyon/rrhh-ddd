<?php

namespace Src\BoundedContext\User\Domain\ValueObject;

/**
 *
 */
class UserName
{
    /**
     * @var string
     */
    private $value;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->value = $name;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }
}
