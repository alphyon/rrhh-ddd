<?php

namespace Src\BoundedContext\User\Domain\ValueObject;

/**
 *
 */
class UserRememberToken
{

    /**
     * @var string|null
     */
    private $value;

    /**
     * @param string|null $userRememberToken
     */
    public function __construct(?string $userRememberToken)
    {
        $this->value = $userRememberToken;
    }

    /**
     * @return string|null
     */
    public function value(): ?string
    {
        return $this->value;
    }
}
