<?php

namespace Src\BoundedContext\User\Domain\ValueObject;

/**
 *
 */
class UserEmail
{
    /**
     * @var string
     */
    private $value;

    /**
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->value = $email;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }
}
