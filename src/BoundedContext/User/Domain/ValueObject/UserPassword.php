<?php

namespace Src\BoundedContext\User\Domain\ValueObject;

/**
 *
 */
class UserPassword
{

    /**
     * @var string
     */
    private $value;

    /**
     * @param string $userPassword
     */
    public function __construct(string $userPassword)
    {
        $this->value = $userPassword;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }
}
