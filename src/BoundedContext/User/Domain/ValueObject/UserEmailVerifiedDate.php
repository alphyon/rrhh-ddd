<?php

namespace Src\BoundedContext\User\Domain\ValueObject;

/**
 *
 */
class UserEmailVerifiedDate
{

    /**
     * @var \DateTime|null
     */
    private $value;

    /**
     * @param \DateTime|null $userEmailVerifiedDate
     */
    public function __construct(?\DateTime $userEmailVerifiedDate)
    {
        $this->value = $userEmailVerifiedDate;
    }

    /**
     * @return \DateTime|null
     */
    public function value(): ?\DateTime
    {
        return $this->value;
    }
}
