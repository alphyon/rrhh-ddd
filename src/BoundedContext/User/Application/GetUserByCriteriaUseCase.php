<?php

namespace Src\BoundedContext\User\Application;

use Src\BoundedContext\User\Domain\Contracts\UserRepositoryContract;
use Src\BoundedContext\User\Domain\User;
use Src\BoundedContext\User\Domain\ValueObject\UserEmail;
use Src\BoundedContext\User\Domain\ValueObject\UserName;

/**
 *
 */
class GetUserByCriteriaUseCase
{
    /**
     * @var UserRepositoryContract
     */
    private $repository;

    /**
     * @param UserRepositoryContract $repository
     */
    public function __construct(UserRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param mixed $userName
     * @param mixed $userEmail
     * @return User|null
     */
    public function __invoke(mixed $userName, mixed $userEmail)
    {
        $name = new UserName($userName);
        $email = new UserEmail($userEmail);
        $user = $this->repository->findByCriteria($name, $email);
        return $user;
    }
}
