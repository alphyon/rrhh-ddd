<?php
declare(strict_types=1);
namespace Src\BoundedContext\User\Application;

use DateTime;
use Src\BoundedContext\User\Domain\Contracts\UserRepositoryContract;
use Src\BoundedContext\User\Domain\User;
use Src\BoundedContext\User\Domain\ValueObject\UserEmail;
use Src\BoundedContext\User\Domain\ValueObject\UserEmailVerifiedDate;
use Src\BoundedContext\User\Domain\ValueObject\UserId;
use Src\BoundedContext\User\Domain\ValueObject\UserName;
use Src\BoundedContext\User\Domain\ValueObject\UserPassword;
use Src\BoundedContext\User\Domain\ValueObject\UserRememberToken;

/**
 *
 */
class UpdateUserUseCase
{

    /**
     * @var UserRepositoryContract
     */
    private $repository;

    /**
     * @param UserRepositoryContract $repository
     */
    public function __construct(UserRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $userId
     * @param string $userName
     * @param string $userEmail
     * @param DateTime|null $userEmailVerifiedDate
     * @param string $userPassword
     * @param string|null $userRememberToken
     * @return void
     */
    public function __invoke(int       $userId,
                             string    $userName,
                             string    $userEmail,
                             ?DateTime $userEmailVerifiedDate,
                             string    $userPassword,
                             ?string   $userRememberToken): void
    {
        $id = new UserId($userId);
        $name = new UserName($userName);
        $email = new UserEmail($userEmail);
        $emailVerifiedDate = new UserEmailVerifiedDate($userEmailVerifiedDate);
        $password = new UserPassword($userPassword);
        $rememberToken = new UserRememberToken($userRememberToken);

        $user = User::create($name,$email,$emailVerifiedDate,$password,$rememberToken);

        $this->repository->update($id,$user);
    }
}
