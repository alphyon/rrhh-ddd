<?php

namespace Src\BoundedContext\User\Application;

use Src\BoundedContext\User\Domain\Contracts\UserRepositoryContract;
use Src\BoundedContext\User\Domain\User;
use Src\BoundedContext\User\Domain\ValueObject\UserEmail;
use Src\BoundedContext\User\Domain\ValueObject\UserEmailVerifiedDate;
use Src\BoundedContext\User\Domain\ValueObject\UserName;
use Src\BoundedContext\User\Domain\ValueObject\UserPassword;
use Src\BoundedContext\User\Domain\ValueObject\UserRememberToken;

/**
 *
 */
class CreateUserCase
{
    /**
     * @var UserRepositoryContract
     */
    private $repository;


    /**
     * @param UserRepositoryContract $repository
     */
    public function __construct(UserRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param mixed $userName
     * @param mixed $userEmail
     * @param \DateTime|null $userEmailVerifiedDate
     * @param string $userPassword
     * @param $userRememberToken
     * @return void
     */
    public function __invoke(mixed      $userName,
                             mixed      $userEmail,
                             ?\DateTime $userEmailVerifiedDate,
                             string     $userPassword, $userRememberToken): void
    {
        $name = new UserName($userName);
        $email = new UserEmail($userEmail);
        $emailVerifiedDate = new UserEmailVerifiedDate($userEmailVerifiedDate);
        $password = new UserPassword($userPassword);
        $rememberToken = new UserRememberToken($userRememberToken);
        $user = User::create($name, $email, $emailVerifiedDate, $password, $rememberToken);

        $this->repository->save($user);
    }
}
