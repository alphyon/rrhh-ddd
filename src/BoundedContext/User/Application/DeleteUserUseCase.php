<?php

namespace Src\BoundedContext\User\Application;

use Src\BoundedContext\User\Domain\Contracts\UserRepositoryContract;
use Src\BoundedContext\User\Domain\ValueObject\UserId;

/**
 *
 */
class DeleteUserUseCase
{

    /**
     * @var UserRepositoryContract
     */
    private $repository;

    /**
     * @param UserRepositoryContract $repository
     */
    public function __construct(UserRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $userId
     * @return void
     */
    public function __invoke(int $userId)
    {
        $id = new UserId($userId);
        $this->repository->delete($id);
    }
}
