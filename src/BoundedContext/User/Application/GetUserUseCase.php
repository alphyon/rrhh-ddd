<?php

namespace Src\BoundedContext\User\Application;

use Src\BoundedContext\User\Domain\Contracts\UserRepositoryContract;
use Src\BoundedContext\User\Domain\User;
use Src\BoundedContext\User\Domain\ValueObject\UserId;

/**
 *
 */
class GetUserUseCase
{

    /**
     * @var UserRepositoryContract
     */
    private $repository;

    /**
     * @param UserRepositoryContract $repository
     */
    public function __construct(UserRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $userId
     * @return User|null
     */
    public function __invoke(int $userId): ?User
    {
        $id = new UserId($userId);
        $user = $this->repository->find($id);
        return $user;
    }
}
