<?php
declare(strict_types=1);

namespace Src\BoundedContext\User\Infrastructure;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Src\BoundedContext\User\Application\CreateUserCase;
use Src\BoundedContext\User\Application\GetUserByCriteriaUseCase;
use Src\BoundedContext\User\Infrastructure\Repositories\EloquentUserRepository;

/**
 *
 */
class CreateUserController
{
    /**
     * @var EloquentUserRepository
     */
    private $repository;

    /**
     * @param EloquentUserRepository $repository
     */
    public function __construct(EloquentUserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return \Src\BoundedContext\User\Domain\User|null
     */
    public function __invoke(Request $request)
    {
        $userName = $request->input('name');
        $userEmail = $request->input('email');
        $userEmailVerifiedDate = null;
        $userPassword = Hash::make($request->input('password'));
        $userRememberToken = null;

        $createUserCase = new CreateUserCase($this->repository);
        $createUserCase->__invoke(
            $userName, $userEmail, $userEmailVerifiedDate, $userPassword, $userRememberToken
        );

        $getUserByCriteriaUseCase = new GetUserByCriteriaUseCase($this->repository);
        $newUser = $getUserByCriteriaUseCase->__invoke(
            $userName,
            $userEmail,
        );
        return $newUser;
    }
}
