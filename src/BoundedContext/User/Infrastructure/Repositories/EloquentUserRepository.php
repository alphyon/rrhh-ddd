<?php

namespace Src\BoundedContext\User\Infrastructure\Repositories;

use App\Models\User as EloquentUserModel;
use Src\BoundedContext\User\Domain\Contracts\UserRepositoryContract;
use Src\BoundedContext\User\Domain\User;
use Src\BoundedContext\User\Domain\ValueObject\UserEmail;
use Src\BoundedContext\User\Domain\ValueObject\UserEmailVerifiedDate;
use Src\BoundedContext\User\Domain\ValueObject\UserId;
use Src\BoundedContext\User\Domain\ValueObject\UserName;
use Src\BoundedContext\User\Domain\ValueObject\UserPassword;
use Src\BoundedContext\User\Domain\ValueObject\UserRememberToken;

/**
 *
 */
final class EloquentUserRepository implements UserRepositoryContract
{
    /**
     * @var EloquentUserModel
     */
    private EloquentUserModel $eloquentUserModel;

    /**
     *
     */
    public function __construct()
    {
        $this->eloquentUserModel = new EloquentUserModel;
    }

    /**
     * @param UserId $id
     * @return User|null
     */
    public function find(UserId $id): ?User
    {
        $user = $this->eloquentUserModel->findOrFail($id->value());
        return new User(
            new UserName($user->name),
            new UserEmail($user->email),
            new UserEmailVerifiedDate($user->email_verified_at),
            new UserPassword($user->password),
            new UserRememberToken($user->remember_token)
        );
    }

    /**
     * @param UserName $name
     * @param UserEmail $email
     * @return User|null
     */
    public function findByCriteria(UserName $name, UserEmail $email): ?User
    {
        $user = $this->eloquentUserModel
            ->where('name', $name->value())
            ->where('email', $email->value())
            ->firstOrFail();

        return new User(
            new UserName($user->name),
            new UserEmail($user->email),
            new UserEmailVerifiedDate($user->email_verified_at),
            new UserPassword($user->password),
            new UserRememberToken($user->remember_token)
        );
    }

    /**
     * @param User $user
     * @return void
     */
    public function save(User $user): void
    {
        $newUser = $this->eloquentUserModel;
        $data = [
            'name' => $user->name()->value(),
            'email' => $user->email()->value(),
            'email_verified_at' => $user->emailVerifiedDate()->value(),
            'password' => $user->password()->value(),
            'remember_token' => $user->rememberToken()->value()
        ];
        $newUser->create($data);
    }

    /**
     * @param UserId $userId
     * @param User $user
     * @return void
     */
    public function update(UserId $userId, User $user): void
    {
        $userToUpdate = $this->eloquentUserModel;
        $data = [
            'name' => $user->name()->value(),
            'email' => $user->email()->value()
        ];

        $userToUpdate->findOrFail($userId->value())->update($data);
    }

    /**
     * @param UserId $id
     * @return void
     */
    public function delete(UserId $id): void
    {
        $this->eloquentUserModel
            ->findOrFail($id->value())
            ->delete();
    }
}
