<?php
declare(strict_types=1);
namespace Src\BoundedContext\User\Infrastructure;

use Src\BoundedContext\User\Application\GetUserUseCase;
use Src\BoundedContext\User\Infrastructure\Repositories\EloquentUserRepository;

/**
 *
 */
class GetUserController
{
    /**
     * @var EloquentUserRepository
     */
    private $repository;

    /**
     * @param EloquentUserRepository $repository
     */
    public function __construct(EloquentUserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Src\BoundedContext\User\Domain\User|null
     */
    public function __invoke(\Illuminate\Http\Request $request)
    {
        $userId = (int)$request->id;

        $getUserUseCase = new GetUserUseCase($this->repository);
        $user = $getUserUseCase->__invoke($userId);
        return $user;
    }
}
