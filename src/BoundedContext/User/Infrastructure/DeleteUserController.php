<?php
declare(strict_types=1);
namespace Src\BoundedContext\User\Infrastructure;

use Src\BoundedContext\User\Application\DeleteUserUseCase;
use Src\BoundedContext\User\Infrastructure\Repositories\EloquentUserRepository;

class DeleteUserController
{
    /**
     * @var EloquentUserRepository
     */
    private $repository;

    /**
     * @param EloquentUserRepository $repository
     */
    public function __construct(EloquentUserRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function __invoke(\Illuminate\Http\Request $request)
    {
        $userId = (int)$request->id;

        $deleteUserUseCase = new DeleteUserUseCase($this->repository);
        $deleteUserUseCase->__invoke($userId);
    }
}
